{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0933066c",
   "metadata": {},
   "outputs": [],
   "source": [
    "# pyright: analyzeUnannotatedFunctions=true"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7fb719c6",
   "metadata": {},
   "source": [
    "# Parallelization with TFDS"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "18511fbe",
   "metadata": {},
   "source": [
    "In this week's exercise, we'll go back to the classic cats versus\n",
    "dogs example, but instead of just naively loading the data to train\n",
    "a model, you will be parallelizing various stages of the Extract,\n",
    "Transform and Load processes. In particular, you will be performing\n",
    "the following tasks:\n",
    "1. Parallelize the extraction of the stored TFRecords of the\n",
    "cats_vs_dogs dataset by using the interleave operation.\n",
    "2. Parallelize the transformation during the preprocessing of the\n",
    "raw dataset by using the map operation.\n",
    "3. Cache the processed dataset in memory by using the cache\n",
    "operation for faster retrieval.\n",
    "4. Parallelize the loading of the cached dataset during the training\n",
    "cycle by using the prefetch operation."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e0b502f9",
   "metadata": {},
   "source": [
    "## Setup"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d2d23fa0",
   "metadata": {},
   "outputs": [],
   "source": [
    "import multiprocessing\n",
    "from os import getcwd\n",
    "\n",
    "import tensorflow as tf\n",
    "import tensorflow_datasets as tfds"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "100cff81",
   "metadata": {
    "lines_to_next_cell": 2
   },
   "source": [
    "## Create and Compile the Model"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8c7bc18a",
   "metadata": {},
   "outputs": [],
   "source": [
    "def create_model():\n",
    "    input_layer = tf.keras.layers.Input(shape=(224, 224, 3))\n",
    "    base_model = tf.keras.applications.MobileNetV2(\n",
    "        input_tensor=input_layer,\n",
    "        weights=\"imagenet\",\n",
    "        include_top=False,\n",
    "    )\n",
    "    base_model.trainable = False\n",
    "\n",
    "    x = tf.keras.layers.GlobalAveragePooling2D()(base_model.output)\n",
    "    x = tf.keras.layers.Dense(2, activation=\"softmax\")(x)\n",
    "\n",
    "    model = tf.keras.models.Model(inputs=input_layer, outputs=x)\n",
    "    model.compile(\n",
    "        optimizer=\"Adam\",\n",
    "        loss=\"sparse_categorical_crossentropy\",\n",
    "        metrics=[\"acc\"],\n",
    "    )\n",
    "    return model"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b91b49bd",
   "metadata": {},
   "source": [
    "## Naive Approach\n",
    "\n",
    "Just for comparison, let's start by using the naive approach to\n",
    "Extract, Transform, and Load the data to train the model defined\n",
    "above. By naive approach we mean that we won't apply any of the new\n",
    "concepts of parallelization that we learned about in this module."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "be78b451",
   "metadata": {},
   "outputs": [],
   "source": [
    "dataset_name = \"cats_vs_dogs\"\n",
    "\n",
    "# If you are running on your local machine\n",
    "# filePath = f\"{getcwd()}/../data\"\n",
    "\n",
    "# If you are running on Coursera or Colab\n",
    "filePath = f\"{getcwd()}/data\"\n",
    "\n",
    "dataset, info = tfds.load(\n",
    "    name=dataset_name,\n",
    "    split=\"train\",\n",
    "    with_info=True,\n",
    "    data_dir=filePath,\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4bcf40db",
   "metadata": {},
   "outputs": [],
   "source": [
    "print(info.version)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "74c401c7",
   "metadata": {},
   "outputs": [],
   "source": [
    "def preprocess(features):\n",
    "    image = features[\"image\"]\n",
    "    image = tf.image.resize(image, (224, 224))\n",
    "    image = image / 255.0\n",
    "    return image, features[\"label\"]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "67768448",
   "metadata": {},
   "outputs": [],
   "source": [
    "train_dataset = dataset.map(preprocess).batch(32)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1a6cfb62",
   "metadata": {},
   "source": [
    "The next step will be to train the model using the following code:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "982ab407",
   "metadata": {},
   "outputs": [],
   "source": [
    "# model = create_model()\n",
    "# model.fit(train_dataset, epochs=5)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7debc4ea",
   "metadata": {},
   "source": [
    "# Parallelize Various Stages of the ETL Processes\n",
    "\n",
    "The following exercises are about parallelizing various stages of\n",
    "Extract, Transform and Load processes. In particular, you will be\n",
    "tasked with performing the following tasks:\n",
    "1. Parallelize the extraction of the stored TFRecords of the\n",
    "cats_vs_dogs dataset by using the interleave operation.\n",
    "2. Parallelize the transformation during the preprocessing of the\n",
    "raw dataset by using the map operation.\n",
    "3. Cache the processed dataset in memory by using the cache\n",
    "operation for faster retrieval.\n",
    "4. Parallelize the loading of the cached dataset during the training\n",
    "cycle by using the prefetch operation.\n",
    "\n",
    "We start by creating a dataset of strings corresponding to the\n",
    "`file_pattern` of the TFRecords of the cats_vs_dogs dataset."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a16df307",
   "metadata": {},
   "outputs": [],
   "source": [
    "# If you are running on your local machine\n",
    "# file_pattern = f\"{getcwd()}/../data/{dataset_name}/{info.version}/{dataset_name}-train.tfrecord*\"\n",
    "\n",
    "# If you are running on Coursera or Colab\n",
    "file_pattern = f\"{getcwd()}/data/{dataset_name}/{info.version}/{dataset_name}-train.tfrecord*\"\n",
    "\n",
    "files = tf.data.Dataset.list_files(file_pattern)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e6c5babe",
   "metadata": {},
   "source": [
    "Let's recall that the TFRecord format is a simple format for storing\n",
    "a sequence of binary records. This is very useful because by\n",
    "serializing the data and storing it in a set of files (100-200MB\n",
    "each) that can each be read linearly greatly increases the\n",
    "efficiency when reading the data.\n",
    "\n",
    "Since we will use it later, we should also recall that a\n",
    "`tf.Example` message (or protobuf) is a flexible message type that\n",
    "represents a `{\"string\": tf.train.Feature}` mapping."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5fb9a2b0",
   "metadata": {},
   "source": [
    "## Parallelize Extraction\n",
    "\n",
    "In the cell below you will use the `interleave` operation with\n",
    "certain arguments to parallelize the extraction of the stored\n",
    "TFRecords of the cats_vs_dogs dataset.\n",
    "\n",
    "Recall that `tf.data.experimental.AUTOTUNE` will delegate the\n",
    "decision about what level of parallelism to use to the `tf.data`\n",
    "runtime."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "abc4148b",
   "metadata": {},
   "outputs": [],
   "source": [
    "# EXERCISE: Parallelize the extraction of the stored TFRecords of the\n",
    "# cats_vs_dogs dataset by using the interleave operation with\n",
    "# cycle_length = 4 and the number of parallel calls set to\n",
    "# tf.data.experimental.AUTOTUNE.\n",
    "train_dataset = files.interleave(\n",
    "    tf.data.TFRecordDataset,\n",
    "    cycle_length=4,\n",
    "    num_parallel_calls=tf.data.experimental.AUTOTUNE,\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "80061f0b",
   "metadata": {},
   "source": [
    "## Parse and Decode\n",
    "\n",
    "At this point the `train_dataset` contains serialized\n",
    "`tf.train.Example` messages. When iterated over, it returns these as\n",
    "scalar string tensors. The sample output for one record is given\n",
    "below:\n",
    "\n",
    "```\n",
    "<tf.Tensor: id=189, shape=(), dtype=string, numpy=b'\\n\\x8f\\xc4\\x01\\n\\x0e\\n\\x05label\\x12\\x05\\x1a\\x03\\n\\x01\\x00\\n,\\n\\x0eimage/filename\\x12\\x1a\\n\\x18\\n\\x16PetImages/Cat/4159.jpg\\n\\xcd\\xc3\\x01\\n\\x05image\\x12...\\xff\\xd9'>\n",
    "```\n",
    "\n",
    "In order to be able to use these tensors to train our model, we must\n",
    "first parse them and decode them. We can parse and decode these\n",
    "string tensors by using a function. In the cell below you will\n",
    "create a `read_tfrecord` function that will read the serialized\n",
    "`tf.train.Example` messages and decode them. The function will also\n",
    "normalize and resize the images after they have been decoded.\n",
    "\n",
    "In order to parse the `tf.train.Example` messages we need to create\n",
    "a `feature_description` dictionary. We need the\n",
    "`feature_description` dictionary because TFDS uses graph-execution\n",
    "and therefore, needs this description to build their shape and type\n",
    "signature. The basic structure of the `feature_description`\n",
    "dictionary looks like this:\n",
    "\n",
    "```python\n",
    "feature_description = {'feature': tf.io.FixedLenFeature([], tf.Dtype, default_value)}\n",
    "```\n",
    "\n",
    "The number of features in your `feature_description` dictionary will\n",
    "vary depending on your dataset. In our particular case, the features\n",
    "are `image` and `label`, and can be seen in the sample output of the\n",
    "string tensor above. Therefore, our `feature_description` dictionary\n",
    "will look like this:\n",
    "\n",
    "```python\n",
    "feature_description = {\n",
    "    'image': tf.io.FixedLenFeature((), tf.string, \"\"),\n",
    "    'label': tf.io.FixedLenFeature((), tf.int64, -1),\n",
    "}\n",
    "```\n",
    "\n",
    "where we have given the default values of `\"\"` and `-1` to the\n",
    "`image` and `label` respectively.\n",
    "\n",
    "The next step will be to parse the serialized `tf.train.Example`\n",
    "message using the `feature_description` dictionary given above. This\n",
    "can be done with the following code:\n",
    "\n",
    "```python\n",
    "example = tf.io.parse_single_example(serialized_example, feature_description)\n",
    "```\n",
    "\n",
    "Finally, we can decode the image by using:\n",
    "\n",
    "```python\n",
    "image = tf.io.decode_jpeg(example['image'], channels=3)\n",
    "```\n",
    "\n",
    "Use the code given above to complete the exercise below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fa59c639",
   "metadata": {},
   "outputs": [],
   "source": [
    "# EXERCISE: Fill in the missing code below.\n",
    "\n",
    "\n",
    "def read_tfrecord(serialized_example):\n",
    "    feature_description = {\n",
    "        \"image\": tf.io.FixedLenFeature((), tf.string, \"\"),\n",
    "        \"label\": tf.io.FixedLenFeature((), tf.int64, -1),\n",
    "    }\n",
    "\n",
    "    # Parse serialized_example and decode the image\n",
    "    example = tf.io.parse_single_example(\n",
    "        serialized_example,\n",
    "        feature_description,\n",
    "    )\n",
    "    image = tf.io.decode_jpeg(example[\"image\"], channels=3)\n",
    "\n",
    "    # Pre-process image\n",
    "    image = tf.cast(image, tf.float32)\n",
    "    image = tf.image.resize(image, (224, 224))\n",
    "    image = image / 255.0\n",
    "\n",
    "    return image, example[\"label\"]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ec5c2cb3",
   "metadata": {},
   "source": [
    "## Parallelize Transformation\n",
    "\n",
    "You can now apply the `read_tfrecord` function to each item in the\n",
    "`train_dataset` by using the `map` method. You can parallelize the\n",
    "transformation of the `train_dataset` by using the `map` method with\n",
    "the `num_parallel_calls` set to the number of CPU cores."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "81c3b967",
   "metadata": {},
   "outputs": [],
   "source": [
    "# EXERCISE: Fill in the missing code below.\n",
    "\n",
    "# Get the number of CPU cores.\n",
    "cores = multiprocessing.cpu_count()\n",
    "print(cores)\n",
    "\n",
    "# Parallelize the transformation of the train_dataset by using the map\n",
    "# operation with the number of parallel calls set to the number of CPU\n",
    "# cores.\n",
    "train_dataset = train_dataset.map(\n",
    "    read_tfrecord,\n",
    "    num_parallel_calls=cores,\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3b70f389",
   "metadata": {},
   "source": [
    "## Cache the Dataset"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3c5f4c76",
   "metadata": {},
   "outputs": [],
   "source": [
    "# EXERCISE: Cache the train_dataset in-memory.\n",
    "train_dataset = train_dataset.cache()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5e6d3809",
   "metadata": {},
   "source": [
    "## Parallelize Loading"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d66b2e00",
   "metadata": {},
   "outputs": [],
   "source": [
    "# EXERCISE: Fill in the missing code below.\n",
    "\n",
    "# Shuffle and batch the train_dataset. Use a buffer size of 1024 for\n",
    "# shuffling and a batch size 32 for batching.\n",
    "train_dataset = train_dataset.shuffle(1024).batch(32)\n",
    "\n",
    "# Parallelize the loading by prefetching the train_dataset. Set the\n",
    "# prefetching buffer size to tf.data.experimental.AUTOTUNE.\n",
    "train_dataset = train_dataset.prefetch(tf.data.experimental.AUTOTUNE)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a99b4c30",
   "metadata": {},
   "source": [
    "The next step will be to train your model using the following code:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "58728e63",
   "metadata": {},
   "outputs": [],
   "source": [
    "# model = create_model()\n",
    "# model.fit(train_dataset, epochs=5)"
   ]
  }
 ],
 "metadata": {
  "jupytext": {
   "cell_metadata_filter": "-all",
   "main_language": "python",
   "notebook_metadata_filter": "-all"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
