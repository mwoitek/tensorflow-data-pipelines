# pyright: analyzeUnannotatedFunctions=true

# %% [markdown]
# ### Step 0 - Setup

# %%
from os import getcwd

import tensorflow as tf
import tensorflow_datasets as tfds
from tensorflow.python.keras.layers import Conv2D
from tensorflow.python.keras.layers import Dense
from tensorflow.python.keras.layers import Flatten
from tensorflow.python.keras.layers import MaxPooling2D

# %% [markdown]
# ### Step 1 - One-Hot Encoding
#
# Remember to one-hot encode the labels, as you have 3 classes - rock,
# paper and scissors. You can use TensorFlow's `one_hot` function to
# convert categorical variables to one-hot vectors.


# %%
# EXERCISE: Encoding the labels using your own function for one-hot
# encoding
def my_one_hot(feature, label):
    one_hot = tf.one_hot(label, depth=3)
    return feature, one_hot


# Testing the function
_, one_hot = my_one_hot(
    ["a", "b", "c", "a"],
    [1, 2, 3, 1],
)
print(one_hot)

# %% [markdown]
# #### Expected Output
# ```
# tf.Tensor(
# [[0. 1. 0.]
#  [0. 0. 1.]
#  [0. 0. 0.]
#  [0. 1. 0.]], shape=(4, 3), dtype=float32)
# ```

# %% [markdown]
# ### Step 2 - Loading Dataset
#
# You will be using `tfds.load()` to load the dataset. The dataset is
# already downloaded and unzipped for you in the data folder. But if you
# are running on your local machine and do not have the dataset downloaded,
# it will first download and save the dataset to your TensorFlow directory
# and then load it.

# %%
# EXERCISE: Loading the rock, paper and scissors train and test datasets
# using tfds.load
filePath = f"{getcwd()}/data"

train_data = tfds.load(
    name="rock_paper_scissors:3.0.0",
    split="train",
    as_supervised=True,
    data_dir=filePath,
)
val_data = tfds.load(
    name="rock_paper_scissors:3.0.0",
    split="test",
    as_supervised=True,
    data_dir=filePath,
)

# Testing that train_data and val_data were loaded correctly
train_data_len = len(list(train_data))
print(train_data_len)

val_data_len = len(list(val_data))
print(val_data_len)

# %% [markdown]
# #### Expected Output
# ```
# 2520
# 372
# ```

# %% [markdown]
# ### Step 3 - Mapping one hot encode function to dataset
#
# You will apply the `my_one_hot()` encoding function to the train and
# validation data using `map`. It will apply the custom function to each
# element of the dataset and return a new dataset containing the
# transformed elements in the same order as they appeared in the input.

# %%
# EXERCISE: One-hot encode the train and validation labels using the
# function you defined earlier
train_data = train_data.map(my_one_hot)
val_data = val_data.map(my_one_hot)

print(type(train_data))

# %% [markdown]
# #### Expected Output
# ```
# <class 'tensorflow.python.data.ops.dataset_ops.MapDataset'>
# ```

# %% [markdown]
# ### Step 4 - Exploring dataset metadata
#
# Do remember that `tfds.load()` has a parameter called `with_info` which
# if True will return the tuple (`tf.data.Dataset`,
# `tfds.core.DatasetInfo`) containing the info associated with the builder.

# %%
# EXERCISE: Check the information about the dataset to see the dataset
# image shape
_, info = tfds.load(
    name="rock_paper_scissors:3.0.0",
    split="test",
    as_supervised=True,
    data_dir=filePath,
    with_info=True,
)

print(info.features["image"].shape)

# %% [markdown]
# ## Expected Output
# ```
# (300, 300, 3)
# ```

# %% [markdown]
# ### Step 5 - Training your simple CNN classifier
#
# Now you will define a simple 1-layer CNN model which will learn to
# classify the images into rock, paper and scissor!

# %%
# EXERCISE: Train a simple CNN model on the dataset
train_batches = train_data.shuffle(100).batch(10)
validation_batches = val_data.batch(32)

input_shape = info.features["image"].shape

model = tf.keras.models.Sequential(
    [
        Conv2D(16, (3, 3), activation="relu", input_shape=input_shape),
        MaxPooling2D(2, 2),
        Flatten(),
        Dense(3, activation="softmax"),
    ]
)
model.build(input_shape)
model.summary()

# %% [markdown]
# ### [Optional] Step 6 - Evaluation

# %%
# OPTIONAL EXERCISE: Compile and fit your model - use categorical loss and
# choose optimizer as Adam

# You should get decent enough training accuracy in 3-4 epochs as this one
# layer model will heavily overfit
epochs = 3

model.compile(
    optimizer="Adam",
    loss="categorical_crossentropy",
    metrics=["accuracy"],
)

history = model.fit(
    train_batches,
    epochs=epochs,
    validation_data=validation_batches,
    validation_steps=1,
)

print(f'Final Training Accuracy: {history.history["accuracy"][-1]}')
print(f'Final Validation Accuracy: {history.history["val_accuracy"][-1]}')
