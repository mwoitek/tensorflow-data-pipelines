# pyright: analyzeUnannotatedFunctions=true

# %% [markdown]
# # Horses or Humans

# %%
from pathlib import Path

import tensorflow as tf
import tensorflow_datasets as tfds
from tensorflow.python.keras.layers import Conv2D
from tensorflow.python.keras.layers import Dense
from tensorflow.python.keras.layers import Flatten
from tensorflow.python.keras.layers import MaxPooling2D

# %%
data_dir = Path(".").resolve().parent / "data"

data = tfds.load(
    "horses_or_humans",
    split="train",
    as_supervised=True,
    data_dir=data_dir,
)
val_data = tfds.load(
    "horses_or_humans",
    split="test",
    as_supervised=True,
    data_dir=data_dir,
)


# %%
def change_image_type(image, label):
    return tf.cast(image, tf.float16), label


# %%
data = data.map(
    change_image_type,
    num_parallel_calls=tf.data.AUTOTUNE,
)
val_data = val_data.map(
    change_image_type,
    num_parallel_calls=tf.data.AUTOTUNE,
)

# %%
train_batches = data.shuffle(100).batch(32)
validation_batches = val_data.batch(32)

# %%
model = tf.keras.models.Sequential(
    [
        Conv2D(16, (3, 3), activation="relu", input_shape=(300, 300, 3)),
        MaxPooling2D(2, 2),
        Conv2D(32, (3, 3), activation="relu"),
        MaxPooling2D(2, 2),
        Conv2D(64, (3, 3), activation="relu"),
        MaxPooling2D(2, 2),
        Conv2D(64, (3, 3), activation="relu"),
        MaxPooling2D(2, 2),
        Conv2D(64, (3, 3), activation="relu"),
        MaxPooling2D(2, 2),
        Flatten(),
        Dense(512, activation="relu"),
        Dense(1, activation="sigmoid"),
    ]
)
model.compile(
    optimizer="Adam",
    loss="binary_crossentropy",
    metrics=["accuracy"],
)

# %%
history = model.fit(
    train_batches,
    epochs=3,
    validation_data=validation_batches,
    validation_steps=1,
)
