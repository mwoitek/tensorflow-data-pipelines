# %% [markdown]
# # TFDS Hello World
#
# In this notebook we will take a look at the simple Hello World scenario
# of TensorFlow Datasets (TFDS). We'll use TFDS to perform the extract,
# transform, and load processes for the MNIST dataset.
#
# ## Setup
#
# We'll start by importing TensorFlow, TensorFlow Datasets, and Matplotlib.

# %%
from pathlib import Path

import matplotlib.pyplot as plt
import tensorflow as tf
import tensorflow_datasets as tfds

# %%
print(f"Using TensorFlow version: {tf.__version__}")

# %%
plt.ion()

# %% [markdown]
# ## Extract - Transform - Load (ETL)
#
# Now we'll run the **ETL** code. First, to perform the **Extract** process
# we use `tfts.load`. This handles everything from downloading the raw data
# to parsing and splitting it, giving us a dataset. Next, we perform the
# **Transform** process. In this simple example, our transform process will
# just consist of shuffling the dataset. Finally, we **Load** one record by
# using the `take(1)` method. In this case, each record consists of an
# image and its corresponding label. After loading the record we proceed to
# plot the image and print its corresponding label.

# %%
# Extract
data_dir = Path(".").resolve().parent / "data"
dataset = tfds.load(
    name="mnist",
    split="train",
    data_dir=data_dir,
)

# %%
# Transform
dataset.shuffle(100)

# %%
# Load
for data in dataset.take(1):
    image = data["image"].numpy().squeeze()
    label = data["label"].numpy()

    print(f"Label: {label}")
    plt.imshow(
        image,
        cmap=plt.cm.binary,  # pyright: ignore
    )
    plt.show()
