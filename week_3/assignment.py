# pyright: analyzeUnannotatedFunctions=true

# %% [markdown]
# # Classify Structured Data
# ## Import TensorFlow and Other Libraries

# %%
from os import getcwd

import pandas as pd
import tensorflow as tf
from sklearn.model_selection import train_test_split
from tensorflow import feature_column
from tensorflow.python.keras import layers

# %% [markdown]
# ## Use Pandas to Create a Dataframe
#
# We will use Pandas to download the dataset and load it into a DataFrame.

# %%
# If you are running your notebook on Coursera
# filePath = f"{getcwd()}/data/heart.csv"

# If you are running your notebook on your local machine
filePath = f"{getcwd()}/../data/heart.csv"

# If you are running your notebook on Colab
# !wget 'https://drive.google.com/uc?export=download&id=109ZN30t5Aa0wq1OuwdE1klBZk4nCHtN2'
# filePath = "uc?export=download&id=109ZN30t5Aa0wq1OuwdE1klBZk4nCHtN2"

dataframe = pd.read_csv(filePath)
dataframe.head()

# %% [markdown]
# ## Split the DataFrame into Train, Validation, and Test Sets
#
# The dataset we downloaded was a single CSV file. We will split this into
# train, validation, and test sets.

# %%
train, test = train_test_split(dataframe, test_size=0.2)
train, val = train_test_split(train, test_size=0.2)

print(f"{len(train)} train examples")
print(f"{len(val)} validation examples")
print(f"{len(test)} test examples")

# %% [markdown]
# ## Create an Input Pipeline Using `tf.data`
#
# Next, we will wrap the DataFrames with `tf.data`. This will enable us to
# use feature columns as a bridge to map from the columns in the Pandas
# DataFrame to features used to train the model. If we were working with a
# very large CSV file (so large that it does not fit into memory), we would
# use `tf.data` to read it from disk directly.

# %%
# EXERCISE: Utility method to create a tf.data Dataset from a Pandas
# DataFrame.


def df_to_dataset(dataframe, shuffle=True, batch_size=32):
    dataframe = dataframe.copy()
    labels = dataframe.pop("target")

    ds = tf.data.Dataset.from_tensor_slices((dict(dataframe), labels))

    if shuffle:
        ds = ds.shuffle(100)
    ds = ds.batch(batch_size)

    return ds


# %%
# A small batch size is used for demonstration purposes
batch_size = 5

train_ds = df_to_dataset(train, batch_size=batch_size)
val_ds = df_to_dataset(val, shuffle=False, batch_size=batch_size)
test_ds = df_to_dataset(test, shuffle=False, batch_size=batch_size)

# %% [markdown]
# ## Understand the Input Pipeline
#
# Now that we have created the input pipeline, let's call it to see the
# format of the data it returns. We have used a small batch size to keep
# the output readable.

# %%
for feature_batch, label_batch in train_ds.take(1):
    print("Every feature:", list(feature_batch.keys()))
    print("A batch of ages:", feature_batch["age"])
    print("A batch of targets:", label_batch)

# %% [markdown]
# We can see that the Dataset returns a dictionary of column names (from
# the DataFrame) that map to column values from rows in the DataFrame.

# %% [markdown]
# ## Create Several Types of Feature Columns
#
# TensorFlow provides many types of feature columns. In this section, we
# will create several types of feature columns, and demonstrate how they
# transform a column from the DataFrame.
#
# **Many of the functions and methods used below have been DEPRECATED!!!!!**

# %%
# Getting an example
example_batch = next(iter(train_ds))[0]


# %%
# Utility method to create a feature column and transform a batch of data.
def demo(feature_column):
    feature_layer = tf.keras.layers.DenseFeatures(
        feature_column,
        dtype="float64",
    )
    print(feature_layer(example_batch).numpy())


# %% [markdown]
# ### Numeric Columns
#
# The output of a feature column becomes the input to the model (using the
# demo function defined above, we will be able to see exactly how each
# column from the DataFrame is transformed). A `numeric_column` is the
# simplest type of column. It is used to represent real valued features.

# %%
# EXERCISE: Create a numeric feature column out of 'age' and demo it.
age = feature_column.numeric_column("age")
demo(age)

# %% [markdown]
# In the heart disease dataset, most columns from the DataFrame are
# numeric.

# %% [markdown]
# ### Bucketized Columns
#
# Often, you don't want to feed a number directly into the model, but
# instead split its value into different categories based on numerical
# ranges. Consider raw data that represents a person's age. Instead of
# representing age as a numeric column, we could split the age into several
# buckets using a `bucketized_column`.

# %%
# EXERCISE: Create a bucketized feature column out of 'age' with the
# following boundaries and demo it.
boundaries = [18, 25, 30, 35, 40, 45, 50, 55, 60, 65]
age_buckets = feature_column.bucketized_column(
    age,
    boundaries=boundaries,
)
demo(age_buckets)

# %% [markdown]
# Notice the one-hot values above describe which age range each row
# matches.

# %% [markdown]
# ### Categorical Columns
#
# In this dataset, thal is represented as a string (e.g. 'fixed', 'normal',
# or 'reversible'). We cannot feed strings directly to a model. Instead, we
# must first map them to numeric values. The categorical vocabulary columns
# provide a way to represent strings as a one-hot vector (much like you
# have seen above with age buckets).

# %%
# EXERCISE: Create a categorical vocabulary column out of the above
# mentioned categories with the key specified as 'thal'.
thal = feature_column.categorical_column_with_vocabulary_list(
    "thal",
    vocabulary_list=("fixed", "normal", "reversible"),
)

# EXERCISE: Create an indicator column out of the created categorical
# column.
thal_one_hot = feature_column.indicator_column(thal)
demo(thal_one_hot)

# %% [markdown]
# ### Embedding Columns
#
# Suppose instead of having just a few possible strings, we have thousands
# (or more) values per category. For a number of reasons, as the number of
# categories grow large, it becomes infeasible to train a neural network
# using one-hot encodings. We can use an embedding column to overcome this
# limitation. Instead of representing the data as a one-hot vector of many
# dimensions, an `embedding_column` represents that data as a
# lower-dimensional, dense vector in which each cell can contain any
# number, not just 0 or 1. You can tune the size of the embedding with the
# `dimension` parameter.

# %%
# EXERCISE: Create an embedding column out of the categorical vocabulary
# you just created (thal). Set the size of the embedding to 8, by using the
# dimension parameter.
thal_embedding = feature_column.embedding_column(
    thal,
    dimension=8,
)
demo(thal_embedding)

# %% [markdown]
# ### Hashed Feature Columns
#
# Another way to represent a categorical column with a large number of
# values is to use a `categorical_column_with_hash_bucket`.  This feature
# column calculates a hash value of the input, then selects one of the
# `hash_bucket_size` buckets to encode a string. When using this column,
# you do not need to provide the vocabulary, and you can choose to make the
# number of hash buckets significantly smaller than the number of actual
# categories to save space.

# %%
# EXERCISE: Create a hashed feature column with 'thal' as the key and 1000
# hash buckets.
thal_hashed = feature_column.categorical_column_with_hash_bucket(
    "thal",
    hash_bucket_size=1000,
)
demo(feature_column.indicator_column(thal_hashed))

# %% [markdown]
# ### Crossed Feature Columns
#
# Combining features into a single feature, better known as [feature
# crosses](https://developers.google.com/machine-learning/glossary/#feature_cross),
# enables a model to learn separate weights for each combination of
# features. Here, we will create a new feature that is the cross of age and
# thal. Note that `crossed_column` does not build the full table of all
# possible combinations (which could be very large). Instead, it is backed
# by a `hashed_column`, so you can choose how large the table is.

# %%
# EXERCISE: Create a crossed column using the bucketized column
# (age_buckets), the categorical vocabulary column (thal) previously
# created, and 1000 hash buckets.
crossed_feature = feature_column.crossed_column(
    [age_buckets, thal],
    hash_bucket_size=1000,
)
demo(feature_column.indicator_column(crossed_feature))

# %% [markdown]
# ## Choose Which Columns to Use
#
# We have seen how to use several types of feature columns. Now we will use
# them to train a model. The goal of this exercise is to show you the
# complete code needed to work with feature columns. We have selected a few
# columns to train our model below arbitrarily.
#
# If your aim is to build an accurate model, try a larger dataset of your
# own, and think carefully about which features are the most meaningful to
# include, and how they should be represented.

# %%
dataframe.dtypes

# %% [markdown]
# You can use the above list of column datatypes to map the appropriate
# feature column to every column in the DataFrame.

# %%
# EXERCISE: Fill in the missing code below
feature_columns = []

# Numeric Cols.
numeric_columns = [
    "age",
    "trestbps",
    "chol",
    "thalach",
    "oldpeak",
    "slope",
    "ca",
]
for header in numeric_columns:
    numeric_feature_column = feature_column.numeric_column(header)
    feature_columns.append(numeric_feature_column)

# Bucketized Cols.
age_buckets = feature_column.bucketized_column(
    age,
    boundaries=[18, 25, 30, 35, 40, 45, 50, 55, 60, 65],
)
feature_columns.append(age_buckets)

# Indicator Cols.
thal = feature_column.categorical_column_with_vocabulary_list(
    "thal",
    vocabulary_list=("fixed", "normal", "reversible"),
)
thal_one_hot = feature_column.indicator_column(thal)
feature_columns.append(thal_one_hot)

# Embedding Cols.
thal_embedding = feature_column.embedding_column(
    thal,
    dimension=8,
)
feature_columns.append(thal_embedding)

# Crossed Cols.
crossed_feature = feature_column.crossed_column(
    [age_buckets, thal],
    hash_bucket_size=1000,
)
crossed_feature = feature_column.indicator_column(crossed_feature)
feature_columns.append(crossed_feature)

# %% [markdown]
# ### Create a Feature Layer
#
# Now that we have defined our feature columns, we will use a
# `DenseFeatures` layer to input them to our Keras model.

# %%
# EXERCISE: Create a Keras DenseFeatures layer and pass the feature_columns
# you just created.
feature_layer = tf.keras.layers.DenseFeatures(
    feature_columns,
    dtype="float64",
)

# %% [markdown]
# Earlier, we used a small batch size to demonstrate how feature columns
# worked. We create a new input pipeline with a larger batch size.

# %%
batch_size = 32
train_ds = df_to_dataset(train, batch_size=batch_size)
val_ds = df_to_dataset(val, shuffle=False, batch_size=batch_size)
test_ds = df_to_dataset(test, shuffle=False, batch_size=batch_size)

# %% [markdown]
# ## Create, Compile, and Train the Model

# %%
model = tf.keras.Sequential(
    [
        feature_layer,
        layers.Dense(128, activation="relu"),
        layers.Dense(128, activation="relu"),
        layers.Dense(1, activation="sigmoid"),
    ]
)
model.compile(
    optimizer="Adam",
    loss="binary_crossentropy",
    metrics=["accuracy"],
)
model.fit(train_ds, validation_data=val_ds, epochs=100)

# %%
loss, accuracy = model.evaluate(test_ds)
print("Accuracy", accuracy)
