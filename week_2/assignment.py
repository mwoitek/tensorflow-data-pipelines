# pyright: analyzeUnannotatedFunctions=true

# %% [markdown]
# # TRANSFER LEARNING
#
# In this exercise, you will use the TensorFlow Dataset's Splits API and
# its concepts which you looked at in the week 2 lecture videos.
#
# Also, you will look at some additional ways of loading things using
# TensorFlow Hub using the [cats_vs_dogs v4
# dataset](https://www.tensorflow.org/datasets/catalog/cats_vs_dogs).
#
# Finally, you will use transfer learning using a pretrained feature vector
# from MobileNet to define a new classification model in the end.
#
# Upon completion of the exercise, you will have
# - Loaded a learnt feature set from MobileNet model.
# - Split the cats_vs_dogs dataset in custom train, validation and test
# sets.
# - Shuffled and batched the custom sets.
# - Defined the model which is ready for tranfer learning using the
# MobileNet feature vector.

# %% [markdown]
# ## Step 0 - Import libraries and set up the splits

# %%
from os import getcwd

import tensorflow as tf
import tensorflow_datasets as tfds
import tensorflow_hub as hub

print(tf.__version__)
print(tfds.__version__)

# %% [markdown]
# ## Step 1 - Load the MobileNet model and its features
#
# The next code block will download the [`mobilenet
# model`](https://tfhub.dev/google/tf2-preview/mobilenet_v2/feature_vector/4)
# from TensorFlow Hub, and use its learned features, extracted as
# `feature_extractor` and set to be fine tuned by making them trainable.

# %%
model_selection = ("mobilenet_v2", 224, 1280)
handle_base, pixels, FV_SIZE = model_selection
IMAGE_SIZE = (pixels, pixels)

filePath = f"{getcwd()}/data"

# Use this if you are running the notebook on Coursera
# feature_extractor = hub.KerasLayer(
#     filePath + "/mobilenet_v2_feature_vector",
#     input_shape=IMAGE_SIZE + (3,),
# )
# feature_extractor.trainable = True

# Use this if you are running the notebook on your local machine or Colab
feature_extractor = hub.KerasLayer(
    f"https://tfhub.dev/google/tf2-preview/{handle_base}/feature_vector/4",
    input_shape=IMAGE_SIZE + (3,),
)
feature_extractor.trainable = True

# %% [markdown]
# ## Step 2 - Split the dataset
#
# You need to use subsets of the original
# [cats_vs_dogs](https://www.tensorflow.org/datasets/catalog/cats_vs_dogs)
# data, which is entirely in the 'train' split, i.e., 'train' contains
# 25000 records with 1738 corrupted images. So in total you have 23262
# images.
#
# You will split it up to get
# - the first 10% as the 'new' training set
# - the last 10% as the new validation and test sets, split down the
# middle, i.e.,
#   - the first half of the last 10% is validation (first 5%)
#   - the second half is test (last 5%)
#
# These 3 recordsets should be called `train_examples`,
# `validation_examples` and `test_examples` respectively.

# %%
# EXERCISE: Split the dataset
split = [
    "train[:10%]",
    "train[-10%:-5%]",
    "train[-5%:]",
]
splits, info = tfds.load(
    "cats_vs_dogs:4.*.*",
    split=split,
    with_info=True,
    data_dir=filePath,
)
train_examples, validation_examples, test_examples = splits

# Testing lengths of the data
train_len = len(list(train_examples))
print(train_len)

validation_len = len(list(validation_examples))
print(validation_len)

test_len = len(list(test_examples))
print(test_len)

# %% [markdown]
# Expected Output
# ```
# 2326
# 1163
# 1163
# ```

# %% [markdown]
# ## Step 3 - Shuffle and map the new batches
#
# Now, you will take a few of the examples from the train set and shuffle
# them initially.
#
# Then, you will map a custom function `format_image` that formats the
# image by resizing it first to `(224, 224)` as that is the input image
# size for MobileNet, and post resizing it normalizes the image by dividing
# each pixel by 255.
#
# Finally, you will create train, test and validation batches with size 16
# here because of memory constraints. Do not edit the `BATCH_SIZE` in the
# code cell and while submitting the assignment.

# %%
num_examples = 500
num_classes = 2

# %%
# EXERCISE: Shuffle and map the batches. This will turn the 3 sets into
# batches so you can train and load batches.


def format_image(features):
    image = features["image"]
    image = tf.image.resize(image, IMAGE_SIZE) / 255.0
    return image, features["label"]


BATCH_SIZE = 16

# For training batches, shuffle the examples by num_examples, map using the
# function defined above, and batch using BATCH_SIZE.
train_batches = (
    train_examples.shuffle(num_examples)
    .map(format_image)
    .batch(BATCH_SIZE)
)

# For validation and test batches, just avoid shuffling and follow the rest
# as the training batch example.
validation_batches = validation_examples.map(format_image).batch(
    BATCH_SIZE
)
test_batches = test_examples.map(format_image).batch(BATCH_SIZE)

# %% [markdown]
# ## Step 4 - Define your transfer learning model
#
# Here, you will use the MobileNet feature vector which you loaded before
# from TensorFlow Hub to create a new model for training.
#
# This is a simple model where you are just using the feature vectors and
# adding the final dense layer to get the cat/dog classification.

# %%
# EXERCISE: Define the model
model = tf.keras.Sequential(
    [
        feature_extractor,
        tf.keras.layers.Dense(num_classes, activation="softmax"),
    ]
)
model.build(IMAGE_SIZE + (3,))
model.summary()

# %% [markdown]
# #### Expected output
# ```
# Model: "sequential"
# _________________________________________________________________
# Layer (type)                 Output Shape              Param #
# =================================================================
# keras_layer (KerasLayer)     (None, 1280)              2257984
# _________________________________________________________________
# dense (Dense)                (None, 2)                 2562
# =================================================================
# Total params: 2,260,546
# Trainable params: 2,226,434
# Non-trainable params: 34,112
# ```

# %% [markdown]
# ## [Optional] Step 5 - Training your model
#
# Training is not in the scope of this assignment but you can go ahead and
# train the network to achieve decent accuracy of 90% and above by training
# for epochs less than 5.

# %%
model.compile(
    optimizer="Adam",
    loss="sparse_categorical_crossentropy",
    metrics=["accuracy"],
)

epochs = 5
history = model.fit(
    train_batches,
    epochs=epochs,
    validation_data=validation_batches,
)

# %%
# Evaluate the model on the test batches
eval_results = model.evaluate(test_batches)

for metric, value in zip(model.metrics_names, eval_results):
    print(f"{metric}: {value:.4f}")
