# %% [markdown]
# # Exploring the Splits API
# ## Setup

# %%
# import tensorflow as tf
from pathlib import Path

import tensorflow_datasets as tfds

# %% [markdown]
# ## Exploring the Splits API

# %%
data_dir = Path(".").resolve().parent / "data"

# %%
train_ds, test_ds = tfds.load(
    "mnist:3.*.*",
    split=["train", "test"],
    data_dir=data_dir,
)

# %%
print(len(list(train_ds)))
print(len(list(test_ds)))

# %% [markdown]
# With the slicing API we can use strings to specify the slicing
# instructions. For example, in the cell below we will merge the training
# and test sets by passing the string `'train+test'` to the `split`
# argument.

# %%
combined = tfds.load(
    "mnist:3.*.*",
    split="train+test",
    data_dir=data_dir,
)
print(len(list(combined)))

# %% [markdown]
# We can also use Python style list slices to specify the data we want.
# For example, we can specify that we want to take the first 10,000 records
# of the `train` split with the string `'train[:10000]'`, as shown below:

# %%
first10k = tfds.load(
    "mnist:3.*.*",
    split="train[:10000]",
    data_dir=data_dir,
)
print(len(list(first10k)))

# %% [markdown]
# It also allows us to specify the percentage of the data we want to use.
# For example, we can select the first 20\% of the training set with the
# string `'train[:20%]'`, as shown below:

# %%
first20p = tfds.load(
    "mnist:3.*.*",
    split="train[:20%]",
    data_dir=data_dir,
)
print(len(list(first20p)))

# %% [markdown]
# We can see that `first20p` contains 12,000 records, which is indeed 20\%
# of the total number of records in the training set. Recall that the
# training set contains 60,000 records.
#
# Because the slices are string-based we can use loops, like the ones shown
# below, to slice up the dataset and make some pretty complex splits. For
# example, the loops below create 10 complimentary validation and training
# sets (each loop returns a list with 5 data sets).

# %%
val_ds = tfds.load(
    "mnist:3.*.*",
    split=["train[{}%:{}%]".format(k, k + 20) for k in range(0, 100, 20)],
    data_dir=data_dir,
)

train_ds = tfds.load(
    "mnist:3.*.*",
    split=[
        "train[:{}%]+train[{}%:]".format(k, k + 20)
        for k in range(0, 100, 20)
    ],
    data_dir=data_dir,
)

# %%
val_ds

# %%
train_ds

# %%
print(len(list(val_ds)))
print(len(list(train_ds)))

# %% [markdown]
# We can also compose new datasets by using pieces from different splits.
# For example, we can create a new dataset from the first 10\% of the test
# set and the last 80\% of the training set, as shown below.

# %%
composed_ds = tfds.load(
    "mnist:3.*.*",
    split="test[:10%]+train[-80%:]",
    data_dir=data_dir,
)
print(len(list(composed_ds)))
